package main

import (
	"calculator/initializers"
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
)

func init() {
	initializers.LoadEnvVariables()
}

func HelloWorld(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "Halo Aska!",
	})
}

func Calculator(c *gin.Context) {
	firstOperand := c.Query("firstOperand")
	operator := c.Query("operator")
	secondOperand := c.Query("secondOperand")
	firstToFloat, err_first := strconv.ParseFloat(firstOperand, 64)
	secondToFloat, err_second := strconv.ParseFloat(secondOperand, 64)
	
	var value string
	var status string
	

	if (operator == "addition" || operator == " ") {
		results := firstToFloat + secondToFloat
		operator = "addition"
		value = "You just did " + operator + " and the value is " + fmt.Sprintf("%v", results)
		status = "Success"
	}
	if (operator == "multiply" || operator == "*") {
		results := firstToFloat * secondToFloat
		operator = "multiply"
		value = "You just did " + operator + " and the value is " + fmt.Sprintf("%v", results)
		status = "Success"
	}
	if (operator == "subtraction" || operator == "-") {
		results := firstToFloat - secondToFloat
		operator = "subtraction"
		value = "You just did " + operator + " and the value is " + fmt.Sprintf("%v", results)
		status = "Success"
	}
	if (operator == "division" || operator == "/") {
		results := firstToFloat / secondToFloat
		operator = "division"
		value = "You just did " + operator + " and the value is " + fmt.Sprintf("%v", results)
		status = "Success"
	}
	if (operator == "") {
		value = "Please input the operator"
		status = "Failed"
	}
	if (err_first != nil) {
		value = "Please input the first operand"
		status = "Failed"
	}

	if (err_second != nil) {
		value = "Please input the second operand"
		status = "Failed"
	}

	if (err_second != nil && err_first != nil) {
		value = "Please input operand"
		status = "Failed"
	}

	c.JSON(200, gin.H{
		"message": value,
		"status": status,
	})
}

func main() {
	r := gin.Default()
	r.GET("/", HelloWorld)
	r.GET("/calculate", Calculator)
	r.Run()
}